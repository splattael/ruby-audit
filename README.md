# Ruby 3 audit

See https://gitlab.com/gitlab-org/gitlab/-/issues/369392 and https://docs.google.com/spreadsheets/d/1gweXRsv0MdMXMyejuWpmGmp2T123geq2rlsJrdDh8r0

## What?

This repository helps to audit used gems at GitLab for Ruby 3 compatibility.

It does via:
- [x] Extract gem and version from `Gemfile.lock`
- [x] Lookup source url (if present) via RubyGems.org API per gem
- [x] Checkout source code with corresponding version
- [ ] Check if source code contains CI instructions to test for Ruby 3.0
- [ ] Pin dependencies of this gem which are used on production
- [ ] Run bundle and bundle exec rake (or spec or test) and check exit code
    - Or any other checks like running specific RuboCop rules

## Help

```shell
$ rake

rake <gem name> # Fetch source for this specific gems
rake -T         # List of all gems

List of internal tasks:
rake _:bootstrap           # Bootstrap data
rake _:cleanup             # Cleanup data
rake _:fetch_gemfile_lock  # Fetch latest Gemfile.lock from GitLab repository
rake _:gem_clone           # Clone repositories for all gems
rake _:gem_info            # Fetch gem information from rubygems.org
rake _:help                # Show help
rake _:update              # Perform tasks for updated Gemfile.lock
```

## How to update Gemfile.lock

1. Run `rake _:bootstrap` to fetch latest Gemfile.lock from GitLab repository.
1. Run `rake _:update` to update gem information from rubygems.org.
1. Add changes via `git add gems/`.
1. Commit all changes via `git commit data gems`.

## How to contribute?

- Clone this repository
- Run `rake _:gem_clone` or `rake <gem name>` to check a specific gem
- Create a branch and commit the changes
- Submit an MR
