# frozen_string_literal: true

require_relative 'lib/audit'
require_relative 'lib/audit/gem'

# Load all task comments needed for `rake _:help`
Rake::TaskManager.record_task_metadata = true

DEPS = Audit.dependencies

task default: :'_:help'

namespace :_ do
  desc 'Show help'
  task :help do
    puts <<~HELP
      rake <gem name> # Fetch source for this specific gems
      rake -T         # List of all gems

      List of internal tasks:
    HELP

    tasks = Rake.application.tasks.select { _1.name.start_with?('_') && _1.comment }
    width = tasks.map { _1.name_with_args.length }.max || 10

    tasks.each do |task|
      printf("rake %-#{width}s  # %s\n", task.name_with_args, task.full_comment)
    end
  end

  desc 'Bootstrap data'
  task :bootstrap => [:fetch_gemfile_lock, :parse_gemfile_lock]

  desc 'Fetch latest Gemfile.lock from GitLab repository'
  task :fetch_gemfile_lock do
    Audit.fetch_deps_lock
  end

  task :parse_gemfile_lock => Audit::DEPS_JSON

  rule Audit::DEPS_LOCK do
    Audit.fetch_deps_lock
  end

  rule Audit::DEPS_JSON => Audit::DEPS_LOCK do
    puts "Converting #{Audit::DEPS_LOCK} to #{Audit::DEPS_JSON}..."
    Audit.convert_lock_to_json
  end

  desc 'Cleanup data'
  task :cleanup do
    Dir['gems/*/*/repo'].each { |d| rm_rf d }
  end

  desc 'Fetch gem information from rubygems.org'
  multitask :gem_info => [:parse_gemfile_lock] + DEPS.map(&:rubygems_path)

  desc 'Clone repositories for all gems'
  multitask :gem_clone => [:gem_info] + DEPS.map(&:repo_path)

  desc 'Perform tasks for updated Gemfile.lock'
  task :update => [:gem_info]
end

DEPS.each do |dep|
  desc "Audit gem #{dep.name}"
  task dep.name => [dep.version_path, dep.rubygems_path, dep.repo_path] do
    puts "Gem #{dep.name} is located in #{dep.repo_path}"
  end

  rule dep.version_path do
    mkdir_p dep.version_path
  end

  rule dep.rubygems_path => Audit::DEPS_JSON do |t|
    Rake::Task[dep.version_path].invoke

    Audit.download_rubygems(dep.name, dep.rubygems_path)
  end

  rule dep.repo_path do
    if dep.source_code_uri
      Audit.clone_repo(dep)
    else
      warn "WARN: #{dep.name} has no source code uri"
    end
  end
end
