# frozen_string_literal: true

require 'pathname'

module Audit
  class Gem
    attr_reader :name, :version, :source_code_uri

    def initialize(name, version)
      @name = name
      @version = version
      @path = Pathname.new("gems/#{name}")
    end

    def gem_path
      string @path
    end

    def version_path
      string @path / @version
    end

    def repo_path
      string @path / @version / 'repo'
    end

    def repo_path_tmp
      string @path / @version / '.repo'
    end

    def repo_logfile_path
      string @path / @version / '.repo.log'
    end

    def each_git_clone
      yield 'git', 'clone', '--depth', '1', '--single-branch', '--branch', "v#{version}", source_code_uri, repo_path_tmp
      yield 'git', 'clone', '--depth', '1', '--single-branch', '--branch', version, source_code_uri, repo_path_tmp
      yield 'git', 'clone', source_code_uri, repo_path_tmp
    end

    def source_code_uri
      return @source_code_uri if defined?(@source_code_uri)

      @source_code_uri = find_source_code_uri(rubygems_info)
    end

    def rubygems_info
      @rubygems_info ||= JSON
        .parse(File.read(rubygems_path))
        .merge(rubygems_info_manual)
    end

    def rubygems_path
      string @path / 'rubygems.json'
    end

    def rubygems_manual_path
      string @path / 'rubygems.json.manual'
    end

    private

    def rubygems_info_manual
      return {} unless File.exist?(rubygems_manual_path)

      JSON.parse(File.read(rubygems_manual_path))
    end

    def string(pathname)
      pathname.to_s
    end

    def find_source_code_uri(json)
      source = json.values_at('source_code_uri', 'homepage_uri').compact.reject(&:empty?).first
      source ||= json.values.find { |v| %r{//(gitlab.com|github.com)}.match?(v) if v.is_a?(String) }

      return unless source

      # For example: https://github.com/rails/rails/tree/v7.0.1
      source.sub!(%r{/tree/.*}, '')
      # https://www.github.com/denro/faraday_middleware-multi_json
      source.sub!(%r{://(www|wiki)\.}, '://')
      # https://threez.github.com/ruby-vmstat
      source.sub!(%r{https?://([\w-]+)\.github.com/([\w-]+)}, 'https://github.com/\1/\2')

      source
    end
  end
end
